package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.model.Food;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface FoodRepository extends MongoRepository<Food, String>{
	public Optional<Food> findById(String id);
	List<Food> findByRestaurantId(String restId);
	List<Food> findByWhen(String when);
}
