package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.model.Myorder;


@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface MyorderRepository extends MongoRepository<Myorder, String> {
	public Optional<Myorder> findById(String id);
}
