package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.model.Profile;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface ProfileRepository extends  MongoRepository<Profile, String> {
	public Profile findByEmail(String email);	
	public Profile findByResName(String resName);
	public Profile findByAddress(String address);
	public Profile findByPhone(String phone);
	public Optional<Profile> findById(String id);
	
}
