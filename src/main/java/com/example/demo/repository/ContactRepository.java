package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.model.Contact;


@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface ContactRepository extends MongoRepository<Contact, String>{
	public Optional<Contact> findById(String id);
}
