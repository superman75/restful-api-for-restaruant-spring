package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Food;
import com.example.demo.model.Response;
import com.example.demo.repository.FoodRepository;

@RestController
@CrossOrigin
@RequestMapping("/foods")
public class FoodController {
	private FoodRepository foodRepository;
	
	public FoodController(FoodRepository foodRepository) {
		this.foodRepository = foodRepository;
	}
	
	@GetMapping("/all")
//	@CrossOrigin(origins = "http://localhost:4200")
	public List<Food> getAll(){
		List<Food> foods = this.foodRepository.findAll();
		return foods;
	}
	@GetMapping("/restId/{restId}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public List<Food> getByRestId(@PathVariable("restId") String restId){
		List<Food> foods = this.foodRepository.findByRestaurantId(restId);
		return foods;
	}
	@PutMapping
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response insert(@RequestBody Food food) {
		this.foodRepository.insert(food);
		return new Response("success",food.getRestaurantId());
	}
	
	@PostMapping
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response update(@RequestBody Food food) {
		this.foodRepository.save(food);
		return new Response("success",food.getRestaurantId());
		
	}
	
	@GetMapping("/id/{id}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Optional<Food> getById(@PathVariable("id") String id) {
		return this.foodRepository.findById(id);
		
	}
	
	
	@GetMapping("/when/{when}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public List<Food> getByWhen(@PathVariable("when") String when){
		return this.foodRepository.findByWhen(when);
	}
	
	@DeleteMapping("/{id}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response delete(@PathVariable("id") String id) {
		this.foodRepository.deleteById(id);
		return new Response("success","delete");
	}
	
}
