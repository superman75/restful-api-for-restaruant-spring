package com.example.demo.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Profile;
import com.example.demo.model.Response;
import com.example.demo.repository.ProfileRepository;
@RestController
@CrossOrigin
@RequestMapping("/profile")
public class ProfileController {

	private ProfileRepository profileRepository;

	protected ProfileController(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}
	
	@GetMapping("/all")
//	@CrossOrigin(origins = "http://localhost:4200")
	public List<Profile> getAll(){
		return this.profileRepository.findAll();
	}
	
	
	@PutMapping
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response insert(@RequestBody Profile profile) {
		profile.setResName(profile.getResName().toLowerCase());
		if (this.getByResName(profile.getResName())!=null) return new Response("failed","This restaurant name is already registered.");
		this.profileRepository.insert(profile);
		return new Response("success",profile.getId());
	}
	
	@PostMapping
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response update(@RequestBody Profile profile) {
		profile.setResName(profile.getResName().toLowerCase());
		if (this.getByResName(profile.getResName())!=null) {
			if(profile.getId()!=this.getByResName(profile.getResName()).getId())
			return new Response("failed","This restaurant name is already registered." + profile.getId());
		}
		
		this.profileRepository.save(profile);;
		return new Response("success",profile.getId());
		
	}
	
	@GetMapping("/email/{email}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Profile getByEmail(@PathVariable("email") String email){
		return this.profileRepository.findByEmail(email);
	}
	

	@GetMapping("/resName/{resName}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Profile getByResName(@PathVariable("resName") String resName){
		return this.profileRepository.findByResName(resName);
	}
	

	@GetMapping("/address/{address}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Profile getByAddress(@PathVariable("address") String address){
		return this.profileRepository.findByAddress(address);
	}
	

	@GetMapping("/id/{id}")
	public Optional<Profile> getById(@PathVariable("id") String id){
		return this.profileRepository.findById(id);
	}
	
	@DeleteMapping("/{id}")
//	@CrossOrigin(origins = "http://localhost:4200")
	public Response delete(@PathVariable("id") String id) {
		this.profileRepository.deleteById(id);
		return new Response("success",id);
	}
}
