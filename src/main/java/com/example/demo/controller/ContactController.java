package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Contact;
import com.example.demo.model.Response;
import com.example.demo.repository.ContactRepository;

@RestController
@RequestMapping("/contacts")
public class ContactController {
	private ContactRepository contactRepository;
	
	public ContactController(ContactRepository contactRepository) {
		this.contactRepository = contactRepository;
	}
	
	@GetMapping("/all")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Contact> getAll(){
		List<Contact> foods = this.contactRepository.findAll();
		return foods;
	}
	@GetMapping("/{id}")
	@CrossOrigin(origins = "http://localhost:4200")
	public Optional<Contact> getById(@PathVariable("id") String id) {
		return this.contactRepository.findById(id);
	}
	
	@PutMapping
	@CrossOrigin(origins = "http://localhost:4200")
	public Response insert(@RequestBody Contact contact) {
		this.contactRepository.insert(contact);
		return new Response("success", contact.getRestaurantId());
	}
	
	@PostMapping
	@CrossOrigin(origins = "http://localhost:4200")
	public Response update(@RequestBody Contact contact) {
		this.contactRepository.save(contact);
		return new Response("success", contact.getRestaurantId());
	}
	
	@DeleteMapping("/{id}")
	@CrossOrigin(origins = "http://localhost:4200")
	public Response delete(@PathVariable("id") String id) {
		this.contactRepository.deleteById(id);
		return new Response("success","delete");
	}
}
