package com.example.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Myorder;
import com.example.demo.model.Response;
import com.example.demo.repository.MyorderRepository;

@RestController
@RequestMapping("/myorders")
public class MyorderController {
	private MyorderRepository myorderRepository;
	
	public MyorderController(MyorderRepository myorderRepository) {
		this.myorderRepository = myorderRepository;
	}
	
	@GetMapping("/all")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Myorder> getAll(){
		List<Myorder> foods = this.myorderRepository.findAll();
		return foods;
	}
	
	@PutMapping
	@CrossOrigin(origins = "http://localhost:4200")
	public Response insert(@RequestBody Myorder myorder) {
		this.myorderRepository.insert(myorder);
		return new Response("success",myorder.getRestaurantId());
	}
	
	@PostMapping
	@CrossOrigin(origins = "http://localhost:4200")
	public Response update(@RequestBody Myorder myorder) {
		this.myorderRepository.save(myorder);
		return new Response("success",myorder.getRestaurantId());		
	}
	
	@DeleteMapping("/{id}")
	@CrossOrigin(origins = "http://localhost:4200")
	public Response delete(@PathVariable("id") String id) {
		this.myorderRepository.deleteById(id);
		return new Response("success","delete");
	}
	
}
