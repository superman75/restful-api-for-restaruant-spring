package com.example.demo.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Foods")
public class Food {
	
	@Id
	private String id;	
	String image_url;
	String food_name;
	@Indexed(direction=IndexDirection.ASCENDING)
	float price;
	String description;
	String state;
	String when;
	String created_at;
	String updated_at;
	String restaurantId;
	
	

	public Food() {
		
	}

	public Food(String image_url, String food_name, float price, String description, String state, String when,
			String created_at, String updated_at, String restaurantId) {
		this.image_url = image_url;
		this.food_name = food_name;
		this.price = price;
		this.description = description;
		this.state = state;
		this.when = when;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.restaurantId = restaurantId;	
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getFood_name() {
		return food_name;
	}
	public void setFood_name(String food_name) {
		this.food_name = food_name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getWhen() {
		return when;
	}
	public void setWhen(String when) {
		this.when = when;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	
}
