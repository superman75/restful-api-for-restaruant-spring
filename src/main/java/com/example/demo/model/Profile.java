package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Profile")
public class Profile {
	@Id
	String id;

	String email;
	String phone;
	String about;
	String address;
	float lat;
	float lon;
	String password;
	String resName;
	String resLogo;
	String zipcode;
	
	protected Profile() {

	}
	protected Profile(String email, String phone, String about, String address, float lat, float lon,
			String password, String resName, String resLogo, String zipcode) {

		this.email = email;
		this.phone = phone;
		this.about = about;
		this.address = address;
		this.lat = lat;
		this.lon = lon;
		this.password = password;
		this.resName = resName;
		this.resLogo = resLogo;
		this.zipcode = zipcode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getResName() {
		return resName;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	public String getResLogo() {
		return resLogo;
	}
	public void setResLogo(String resLogo) {
		this.resLogo = resLogo;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
	
	
}
