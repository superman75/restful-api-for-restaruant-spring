package com.example.demo.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Myorders")
public class Myorder {
	@Id
	String id;

	List<Order> orders;
	String user_name;
	String user_phone;
	@Indexed(direction=IndexDirection.ASCENDING)
	String created_at;
	String restaurantId;
	
	
	protected Myorder() {

	}
	public Myorder(List<Order> orders, String user_name, String user_phone, String created_at, String restaurantId) {

		this.orders = orders;
		this.user_name = user_name;
		this.user_phone = user_phone;
		this.created_at = created_at;
		this.restaurantId = restaurantId;
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}
	
	
	
}
