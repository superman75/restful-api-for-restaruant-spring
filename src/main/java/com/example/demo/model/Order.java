package com.example.demo.model;


public class Order {
	private String image_url;
	private String food_name;
	private float price;
	private int count;
	private String reserveDate;
	
	
	
	protected Order() {

	}

	public Order(String image_url, String food_name, float price, int count, String reserveDate) {
		this.image_url = image_url;
		this.food_name = food_name;
		this.price = price;
		this.count = count;
		this.reserveDate = reserveDate;
	}
	
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getFood_name() {
		return food_name;
	}
	public void setFood_name(String food_name) {
		this.food_name = food_name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getReserveDate() {
		return reserveDate;
	}
	public void setReserveDate(String reserveDate) {
		this.reserveDate = reserveDate;
	}
	
	
}
